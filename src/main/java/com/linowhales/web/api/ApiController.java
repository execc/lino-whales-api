package com.linowhales.web.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.linowhales.web.db.TrendingRepository;
import com.linowhales.web.db.UserStatisticsRepository;
import com.linowhales.web.entity.TrendingUserStatistics;
import com.linowhales.web.entity.UserStatistics;
import com.linowhales.web.entity.UserStatisticsId;


@RestController
@ExposesResourceFor(ApiController.class)
@RequestMapping(
		value="/api", 
		produces = {"application/json"},
		method = { RequestMethod.GET })
public class ApiController {

	private UserStatisticsRepository db;
	private TrendingRepository trend;

	@Autowired
	public ApiController(UserStatisticsRepository db, TrendingRepository trend) {
		this.db = db;
		this.trend = trend;
	}

	@Cacheable
	@RequestMapping(value="querySingleStat", name="querySingleStat", method = { RequestMethod.GET })
	public UserStatistics querySingleStat(
			@RequestParam(name="user") String username, 
			@RequestParam(name="date") String date) {

		if (StringUtils.isEmpty(username)) {
			throw new IllegalArgumentException("Invalid username");
		}
		if (StringUtils.isEmpty(date)) {
			throw new IllegalArgumentException("Invalid date");
		}
		
		return db.findById(new UserStatisticsId(username, date))
				.orElseThrow(() -> new IllegalArgumentException("Stat not fond"));
	}	

	@Cacheable
	@RequestMapping(value="queryCurrentStat", name="queryCurrentStat", method = { RequestMethod.GET })
	public Page<UserStatistics> queryCurrentStat(
			@RequestParam(name="page") int page, 
			@RequestParam(name="pageSize") int pageSize, 
			@RequestParam(name="asc") boolean asc, 
			@RequestParam(name="criteria") String column) {
		
		checkPage(page, pageSize);
		
		checkColumn(column);
		
		PageRequest req = PageRequest.of(page, pageSize, asc ? Direction.ASC : Direction.DESC, column);
		UserStatistics stat = db.findFirstByOrderByIdDateDesc();
		return db.findByIdDate(stat.getId().getDate(), req);
	}

	@Cacheable
	@RequestMapping(value="queryDateStat", name="queryDateStat", method = { RequestMethod.GET })
	public Page<UserStatistics> queryDateStat(
			@RequestParam(name="date") String date,
			@RequestParam(name="page") int page, 
			@RequestParam(name="pageSize") int pageSize, 
			@RequestParam(name="asc") boolean asc, 
			@RequestParam(name="criteria") String column) {
		
		if (StringUtils.isEmpty(date)) {
			throw new IllegalArgumentException("Invalid date");
		}
		
		checkPage(page, pageSize);
		
		checkColumn(column);
		
		PageRequest req = PageRequest.of(page, pageSize, asc ? Direction.ASC : Direction.DESC, column);
		return db.findByIdDate(date, req);
	}

	@Cacheable
	@RequestMapping(value="queryHistory", name="queryHistory", method = { RequestMethod.GET })
	public Page<UserStatistics> queryHistory(
			@RequestParam(name="user") String username,
			@RequestParam(name="page") int page, 
			@RequestParam(name="pageSize") int pageSize, 
			@RequestParam(name="asc") boolean asc) {
		
		checkPage(page, pageSize);
		
		PageRequest req = PageRequest.of(page, pageSize, asc ? Direction.ASC : Direction.DESC, "id.date");
		return db.findByIdUsername(username, req);
	}
	
	@Cacheable
	@RequestMapping(value="queryTrending", name="queryTrending", method = { RequestMethod.GET })
	public PageImpl<TrendingUserStatistics> queryTrending(
			@RequestParam(name="period") int periodLength,
			@RequestParam(name="page") int page, 
			@RequestParam(name="pageSize") int pageSize, 
			@RequestParam(name="criteria") String column) {
		
		checkPage(page, pageSize);
		
		checkColumn(column);
		
		return trend.execute(column, periodLength, page, pageSize);
	}
	
	private void checkColumn(String column) {
		try {
			UserStatistics.class.getDeclaredField(column);
		} catch (NoSuchFieldException | SecurityException e) {
			throw new IllegalArgumentException("Invalid column " + column);
		}
	}

	private void checkPage(int page, int pageSize) {
		if (page < 0) {
			throw new IllegalArgumentException("Invalid page");
		}
		
		if (pageSize > 100) {
			throw new IllegalArgumentException("Invalid page size");
		}
	}
		
}
