package com.linowhales.web.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Entity
public class TrendingUserStatistics {

	@EmbeddedId
	private UserStatisticsId id;
	
	@Column(name="lino")
	@Trending
	private double lino;
	
	@Column(name="lp")
	@Trending
	private double lp;
	
	@Column(name="posts")
	@Trending
	private int posts;
	
	@Column(name="followers")
	@Trending
	private int followers;
	
	@Column(name="following")
	@Trending
	private int following;
	
	@Column(name="original_income")
	@Trending
	private double originalIncome;
	
	@Column(name="friction_income")
	@Trending
	private double frictionIncome;
	
	@Column(name="inflation_income")
	@Trending
	private double inflationIncome;

	public UserStatisticsId getId() {
		return id;
	}

	public void setId(UserStatisticsId id) {
		this.id = id;
	}

	public double getLino() {
		return lino;
	}

	public void setLino(double lino) {
		this.lino = lino;
	}

	public double getLp() {
		return lp;
	}

	public void setLp(double lp) {
		this.lp = lp;
	}

	public int getPosts() {
		return posts;
	}

	public void setPosts(int posts) {
		this.posts = posts;
	}

	public int getFollowers() {
		return followers;
	}

	public void setFollowers(int followers) {
		this.followers = followers;
	}

	public int getFollowing() {
		return following;
	}

	public void setFollowing(int following) {
		this.following = following;
	}

	public double getOriginalIncome() {
		return originalIncome;
	}

	public void setOriginalIncome(double originalIncome) {
		this.originalIncome = originalIncome;
	}

	public double getFrictionIncome() {
		return frictionIncome;
	}

	public void setFrictionIncome(double frictionIncome) {
		this.frictionIncome = frictionIncome;
	}

	public double getInflationIncome() {
		return inflationIncome;
	}

	public void setInflationIncome(double inflationIncome) {
		this.inflationIncome = inflationIncome;
	}

	@Column(name="period_start")
	private String periodStart;
	@Column(name="period_end")
	private String periodEnd;

	@Column(name="min")
	private double min;
	@Column(name="max")
	private double max;
	@Column(name="diff")
	private double diff;

	public String getPeriodStart() {
		return periodStart;
	}

	public void setPeriodStart(String periodStart) {
		this.periodStart = periodStart;
	}

	public String getPeriodEnd() {
		return periodEnd;
	}

	public void setPeriodEnd(String periodEnd) {
		this.periodEnd = periodEnd;
	}

	public double getMin() {
		return min;
	}

	public void setMin(double min) {
		this.min = min;
	}

	public double getMax() {
		return max;
	}

	public void setMax(double max) {
		this.max = max;
	}

	public double getDiff() {
		return diff;
	}

	public void setDiff(double diff) {
		this.diff = diff;
	}
}
