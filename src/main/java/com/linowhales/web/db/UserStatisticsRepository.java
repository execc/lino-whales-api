package com.linowhales.web.db;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.linowhales.web.entity.UserStatistics;
import com.linowhales.web.entity.UserStatisticsId;

/**
 * Reposititory for {@link UserStatistics}
 * 
 * @author VasinDI
 *
 */
public interface UserStatisticsRepository extends JpaRepository<UserStatistics, UserStatisticsId> {
	
	/**
	 * Returns first entitiy with most current date 
	 * 
	 * @return first entitiy with most current date
	 */
	UserStatistics findFirstByOrderByIdDateDesc();

	/**
	 * Searches full history by username
	 * 
	 * @param username
	 * @param pageable
	 * @return returns page with user statistics
	 */
	Page<UserStatistics> findByIdUsername(String username, Pageable pageable);
	
	/**
	 * Searches full history by date
	 * 
	 * @param date
	 * @param pageable
	 * @return page with user statistics
	 */
	Page<UserStatistics> findByIdDate(String date, Pageable pageable);

}
