package com.linowhales.web.db;

import java.lang.reflect.Field;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Repository;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;
import com.linowhales.web.entity.Trending;
import com.linowhales.web.entity.TrendingUserStatistics;
import com.linowhales.web.entity.UserStatistics;

/**
 * Repository class with access to 'Trending' page.
 * 
 * Example query
 * <pre>
 * SELECT 
	u4.`period_start`,
	u4.`period_end`,
	u4.`min`,
	u4.`max`,
	u4.`diff`,
	u5.`date`,
	u5.`username`,
	u5.`lino`,
	u5.`lp`,
	u5.`posts`,
	u5.`followers`,
	u5.`following`,
	u5.`original_income`,
	u5.`friction_income`,
	u5.`inflation_income`
FROM (
	SELECT  
		u1.`min_date` as `period_start`,
		u1.`max_date` as `period_end`,
		u1.`username`,
		u2.`lino` as `min`,
		u3.`lino` as `max`,
		u3.`lino` - u2.`lino` as `diff` 
	FROM (	
		SELECT 
			MIN(`date`) as `min_date`, 
			MAX(`date`) as `max_date`,
			`username`,
			DATEDIFF(`date`, NOW()) <= 30 as `period_id`
		FROM `user_statistics`
		WHERE `lino` > 0
		GROUP BY
			DATEDIFF(`date`, NOW()) <= 30,
			`username`
		ORDER BY `username` DESC) as u1
	JOIN `user_statistics` u2 ON
		u1.`min_date` = u2.`date` AND
		u1.`username` = u2.`username`
	JOIN `user_statistics` u3 ON 
		u1.`max_date` = u3.`date` AND
		u1.`username` = u3.`username`
	ORDER BY u3.`lino` - u2.`lino` DESC) as u4
JOIN `user_statistics` u5 ON
	u5.`username` = u4.`username` AND
	u5.`date` = u4.`period_end`
ORDER BY u4.`diff` DESC;
 * </pre>
 * 
 * @author VasinDI
 *
 */
@Repository
public class TrendingRepository {
	
	/**
	 * Query template count
	 */
	private static final String QUERY_COUNT = "COUNT(*)\r\n";
	
	/**
	 * Query template columns
	 */
	private static final String QUERY_COLUMNS = 			
			"	u4.`period_start`,\r\n" + 
			"	u4.`period_end`,\r\n" + 
			"	u4.`min`,\r\n" + 
			"	u4.`max`,\r\n" + 
			"	u4.`diff`,\r\n" + 
			"	u5.`date`,\r\n" + 
			"	u5.`username`,\r\n" + 
			"	u5.`lino`,\r\n" + 
			"	u5.`lp`,\r\n" + 
			"	u5.`posts`,\r\n" + 
			"	u5.`followers`,\r\n" + 
			"	u5.`following`,\r\n" + 
			"	u5.`original_income`,\r\n" + 
			"	u5.`friction_income`,\r\n" + 
			"	u5.`inflation_income`\r\n";
	
	/**
	 * Query template
	 */
	private static final String QUERY_TEMPLATE = "SELECT \r\n" + 
			"	%2$s" + 
			"FROM (\r\n" + 
			"	SELECT  \r\n" + 
			"		u1.`min_date` as `period_start`,\r\n" + 
			"		u1.`max_date` as `period_end`,\r\n" + 
			"		u1.`username`,\r\n" + 
			"		u2.`%1$s` as `min`,\r\n" + 
			"		u3.`%1$s` as `max`,\r\n" + 
			"		u3.`%1$s` - u2.`%1$s` as `diff` \r\n" + 
			"	FROM (	\r\n" + 
			"		SELECT \r\n" + 
			"			MIN(`date`) as `min_date`, \r\n" + 
			"			MAX(`date`) as `max_date`,\r\n" + 
			"			`username`,\r\n" + 
			"			DATEDIFF(`date`, NOW()) <= ?1 as `period_id`\r\n" + 
			"		FROM `user_statistics`\r\n" + 
			"		WHERE `%1$s` > 0\r\n" + 
			"		GROUP BY\r\n" + 
			"			DATEDIFF(`date`, NOW()) <= ?1,\r\n" + 
			"			`username`\r\n" + 
			"		ORDER BY `username` DESC) as u1\r\n" + 
			"	JOIN `user_statistics` u2 ON\r\n" + 
			"		u1.`min_date` = u2.`date` AND\r\n" + 
			"		u1.`username` = u2.`username`\r\n" + 
			"	JOIN `user_statistics` u3 ON \r\n" + 
			"		u1.`max_date` = u3.`date` AND\r\n" + 
			"		u1.`username` = u3.`username`\r\n" + 
			"	ORDER BY u3.`%1$s` - u2.`%1$s` DESC) as u4\r\n" + 
			"JOIN `user_statistics` u5 ON\r\n" + 
			"	u5.`username` = u4.`username` AND\r\n" + 
			"	u5.`date` = u4.`period_end`\r\n" + 
			"ORDER BY u4.`diff` DESC";
	
	private final Map<String, Pair<String, String>> queries;

	private EntityManager em;
	
	/**
	 * Initializes query template for each annotated field
	 */
	@Autowired
	public TrendingRepository(EntityManager em) {
		Builder<String, Pair<String, String>> builder = ImmutableMap.builder();
		for (Field field : UserStatistics.class.getDeclaredFields()) {
			if (field.getAnnotation(Trending.class) != null) {
				Column col = field.getAnnotation(Column.class);
				
				String name = col == null ? field.getName() : col.name();
				String query = String.format(QUERY_TEMPLATE, name, QUERY_COLUMNS);
				String countQuery = String.format(QUERY_TEMPLATE, name, QUERY_COUNT);
				;
				builder.put(name,Pair.of(query, countQuery));
			}
		}
		
		this.queries = builder.build();
		this.em = em;
	}
	
	@SuppressWarnings("unchecked")
	public PageImpl<TrendingUserStatistics> execute(String field, int periodLength, int pageNumber, int pageSize) {
		if (periodLength > 100 || periodLength < 1) {
			throw new IllegalArgumentException("Invalid period " + periodLength);
		}
		
		if (pageSize > 100 || pageSize < 0) {
			throw new IllegalArgumentException("Invalid page size " + pageSize);
		}
		
		if (pageNumber < 0) {
			throw new IllegalArgumentException("Invalid page number " + pageNumber);
		}
		
		Pair<String, String> query = queries.get(field);
		if (query == null) {
			throw new IllegalArgumentException("Invalid field " + field);
		}
		
		Query selectQuery = em.createNativeQuery(query.getFirst(), TrendingUserStatistics.class);
		selectQuery.setParameter(1, periodLength);
		
		selectQuery.setFirstResult(pageNumber * pageSize); 
		selectQuery.setMaxResults(pageSize);
		
		Query countQuery = em.createNativeQuery(query.getSecond());
		countQuery.setParameter(1, periodLength);
		
		BigInteger count = (BigInteger) countQuery.getSingleResult();
		
		List<TrendingUserStatistics> data = selectQuery.getResultList();
		
		PageRequest pageRequest = PageRequest.of(pageNumber, pageSize);
		PageImpl<TrendingUserStatistics> page = new PageImpl<>(data, pageRequest, count.longValue());
		
		return page;
	}
}
