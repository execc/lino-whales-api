package com.linowhales.web.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Entity(name="user_statistics")
public class UserStatistics {
	
	@EmbeddedId
	private UserStatisticsId id;
	
	@Column(name="lino")
	private BigDecimal lino;
	
	@Column(name="lp")
	private BigDecimal lp;
	
	@Column(name="posts")
	private int posts;
	
	@Column(name="followers")
	private int followers;
	
	@Column(name="following")
	private int following;
	
	@Column(name="original_income")
	private BigDecimal originalIncome;
	
	@Column(name="friction_income")
	private BigDecimal frictionIncome;
	
	@Column(name="inflation_income")
	private BigDecimal inflationIncome;

	public UserStatisticsId getId() {
		return id;
	}

	public void setId(UserStatisticsId id) {
		this.id = id;
	}

	public BigDecimal getLino() {
		return lino;
	}

	public void setLino(BigDecimal lino) {
		this.lino = lino;
	}

	public BigDecimal getLp() {
		return lp;
	}

	public void setLp(BigDecimal lp) {
		this.lp = lp;
	}

	public int getPosts() {
		return posts;
	}

	public void setPosts(int posts) {
		this.posts = posts;
	}

	public int getFollowers() {
		return followers;
	}

	public void setFollowers(int followers) {
		this.followers = followers;
	}

	public int getFollowing() {
		return following;
	}

	public void setFollowing(int following) {
		this.following = following;
	}

	public BigDecimal getOriginalIncome() {
		return originalIncome;
	}

	public void setOriginalIncome(BigDecimal originalIncome) {
		this.originalIncome = originalIncome;
	}

	public BigDecimal getFrictionIncome() {
		return frictionIncome;
	}

	public void setFrictionIncome(BigDecimal frictionIncome) {
		this.frictionIncome = frictionIncome;
	}

	public BigDecimal getInflationIncome() {
		return inflationIncome;
	}

	public void setInflationIncome(BigDecimal inflationIncome) {
		this.inflationIncome = inflationIncome;
	}
	
	
}
