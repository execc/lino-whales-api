package com.linowhales.web.api;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.linowhales.web.db.UserStatisticsRepository;
import com.linowhales.web.entity.UserStatistics;
import com.linowhales.web.entity.UserStatisticsId;


@RestController
@ExposesResourceFor(ApiController.class)
@RequestMapping(
		value="/api", 
		produces = {"application/json"},
		method = { RequestMethod.GET })
public class ApiController {

	private UserStatisticsRepository db;

	@Autowired
	public ApiController(UserStatisticsRepository db) {
		this.db = db;
	}

	@RequestMapping(value="queryCurrentStat", name="queryCurrentStat", method = { RequestMethod.GET })
	public Page<UserStatistics> queryCurrentStat(
			@RequestParam(name="page") int page, 
			@RequestParam(name="pageSize") int pageSize, 
			@RequestParam(name="asc") boolean asc, 
			@RequestParam(name="criteria") String column) {
		
		if (page < 1) {
			throw new IllegalArgumentException("Invalid page");
		}
		
		if (pageSize > 100) {
			throw new IllegalArgumentException("Invalid page size");
		}
		
		PageRequest req = PageRequest.of(page, pageSize, asc ? Direction.ASC : Direction.DESC, column);
		UserStatisticsId id = new UserStatisticsId();
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
		id.setDate(fmt.format(new Date()));
		UserStatistics stat = new UserStatistics();
		stat.setId(id);
		return db.findAll(Example.of(stat), req);
	}
	
	@RequestMapping(value="queryHistory", name="queryHistory", method = { RequestMethod.GET })
	public Page<UserStatistics> queryHistory(
			@RequestParam(name="user") String username,
			@RequestParam(name="page") int page, 
			@RequestParam(name="pageSize") int pageSize, 
			@RequestParam(name="asc") boolean asc) {
		
		if (page < 1) {
			throw new IllegalArgumentException("Invalid page");
		}
		
		if (pageSize > 100) {
			throw new IllegalArgumentException("Invalid page size");
		}
		
		PageRequest req = PageRequest.of(page, pageSize, asc ? Direction.ASC : Direction.DESC, "id.date");
		UserStatisticsId id = new UserStatisticsId();
		id.setUsername(username);
		UserStatistics stat = new UserStatistics();
		stat.setId(id);
		return db.findAll(Example.of(stat), req);
	}
}
