package com.linowhales.web.db;

import org.springframework.data.jpa.repository.JpaRepository;

import com.linowhales.web.entity.UserStatistics;
import com.linowhales.web.entity.UserStatisticsId;

/**
 * Reposititory for {@link UserStatistics}
 * 
 * @author VasinDI
 *
 */
public interface UserStatisticsRepository extends JpaRepository<UserStatistics, UserStatisticsId> {

}
